﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
public class GameController : MonoBehaviour
{
    [HideInInspector] public GameStatus GameStatusControl;

    /// <summary>
    /// Toplam score degerini tutar
    /// </summary>
    [HideInInspector] public int TotalScore = 0;

    /// <summary>
    /// kac hamle gerceklesti bunu gosteri
    /// </summary>
    [HideInInspector] public int MoveCount = 0;

    /// <summary>
    /// 1000 puandan sonra bomba gelmesi icin tuttugum deger.
    /// </summary>
    [HideInInspector] public int BombScoreCounter = 0;

    //UI
    [SerializeField] public Text _movesTextUI;
    [SerializeField] public Text _scoreUI;
    [SerializeField] private GameObject _restartUI;
    [SerializeField] private GameObject _gameOverUI;


    void Start()
    {
        GameStatusControl = GameStatus.Empty;
        StartCoroutine(ShowRestartButton());
    }
    private IEnumerator ShowRestartButton()
    {
        yield return new WaitForSeconds(1);
        _restartUI.SetActive(true);
    }
    
    private void Update()
    {
        _scoreUI.text = TotalScore.ToString();
        _movesTextUI.text = MoveCount.ToString();

        if (GameStatusControl == GameStatus.Game_Over)
        {
            DOTween.KillAll(true);
            GameObject.FindGameObjectWithTag("HexagonControllerTag").GetComponent<HexagonController>().enabled=false;
            GameObject.FindGameObjectWithTag("TouchTag").GetComponent<TouchController>().KillMethod();
            _gameOverUI.SetActive(true);
            _gameOverUI.transform.Find("GameOverScore").GetComponent<Text>().text = $" SCORE \n {TotalScore}";
        }
    }
    public void Restart()
    {
        DOTween.KillAll(true);
        GameObject.FindGameObjectWithTag("TouchTag").GetComponent<TouchController>().KillMethod();
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
    
}
