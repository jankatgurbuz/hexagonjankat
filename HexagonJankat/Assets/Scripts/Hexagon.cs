﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Bu class bir hexagonun butun ozelliklerini tutar.
/// </summary>
public class Hexagon
{
    // Komuslarim------------------
    public Hexagon Up;
    public Hexagon Down;
    public Hexagon RightUp;
    public Hexagon RightDown;
    public Hexagon LeftUp;
    public Hexagon LeftDown;
    //------------------------------------------

    /// <summary>
    /// Objenin rengi 
    /// </summary>
    public Color ColorObj;

    /// <summary>
    /// Hexagon Objem
    /// </summary>
    public GameObject HexagonObj;

    // Satir ve Sutun sayisi ---- 
    public int Row;
    public int Column;
    //---------------------------

    /// <summary>
    /// Bir Hexagonun pozisyonu
    /// </summary>
    public Vector3 MyPos;

    /// <summary>
    /// her hexagonda bulunan 6 tane Yon bulmak icin nokta
    /// </summary>
    public Vector3[] MyChilderPos;

    /// <summary>
    /// Color degerim
    /// </summary>
    public int ColorIndex;

    /// <summary>
    /// Hexagonun Sprite degeri
    /// </summary>
    public SpriteRenderer SpriteRenderer;

    /// <summary>
    /// true ise bomba degise hexagon
    /// </summary>
    public bool IsBomb = false;

    /// <summary>
    /// Bomba ise icerisindeki script nesnesi
    /// </summary>
    public HexagonBomb HexagonBomb;

    public Hexagon(GameObject hexagonObj, Color hexagonColor, int row, int column, int colorIndex)
    {
        ColorIndex = colorIndex;
        HexagonObj = hexagonObj;
        Row = row;
        Column = column;
        MyChilderPos = new Vector3[6];
        if (HexagonObj != null)
        {
            HexagonObj.GetComponent<SpriteRenderer>().color = hexagonColor;
            ColorObj = hexagonColor;
            HexagonObj.name = $"{Row}-{Column}";
            SpriteRenderer = HexagonObj.GetComponent<SpriteRenderer>();
        }
    }
    public Hexagon(GameObject hexagonObj, Color hexagonColor, int row, int column, Vector3[] corners, Vector3 myPos, int colorIndex)
    {
        ColorIndex = colorIndex;
        MyPos = myPos;
        HexagonObj = hexagonObj;
        Row = row;
        Column = column;
        MyChilderPos = corners;
        if (HexagonObj != null)
        {
            HexagonObj.GetComponent<SpriteRenderer>().color = hexagonColor;
            ColorObj = hexagonColor;
            HexagonObj.name = $"{Row}-{Column}";
            SpriteRenderer = HexagonObj.GetComponent<SpriteRenderer>();
        }
        hexagonObj.transform.position = MyPos;

        //Debug.Log($"{row},{column}   myPos{myPos} colorIndex{ColorIndex}");


    }


}
