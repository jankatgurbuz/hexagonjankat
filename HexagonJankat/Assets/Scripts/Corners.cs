﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corners 
{
    public Vector3 []CornerPositions;
    public Corners(Vector3 []cornerPositions)
    {
        CornerPositions = cornerPositions;
    }
}
