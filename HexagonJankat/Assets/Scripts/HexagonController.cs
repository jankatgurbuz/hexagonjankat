﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public partial class HexagonController : MonoBehaviour
{
    #region Degiskenler
    /// <summary>
    /// Olusturulacak hexagon prefabi
    /// </summary>
    [SerializeField] private GameObject _hexagonPrefab;

    /// <summary>
    /// Olusturulacak bomba hexagon prefabi
    /// </summary>
    [SerializeField] private GameObject _hexagonPrefabBomb;

    /// <summary>
    /// _hexagon rengi ve satir,sutun sayilarini tutan scriptable object.
    ///  Ayrica belirlenen sayida random renk uretecek metodda sahiptir.
    /// </summary>
    [SerializeField] private HexagonProperty _hexagonProperty;

    /// <summary>
    /// Oyunun temel grid yapisi
    /// </summary>
    [SerializeField] private Grid grid;

    /// <summary>
    /// Oyunun ana dizisidir.Butun hexagon bilgileri bu dizide tutulur.
    /// </summary>
    [HideInInspector] public Hexagon[,] _hexagon;

    /// <summary>
    /// Her hexagonun 6 temel noktasi vardir bu noktalari tutan iki boyutlu bir dizi.
    /// </summary>
    private Corners[,] _corners;

    /// <summary>
    /// Oyununun temel yonetimini saglayan nesnedir.
    /// </summary>
    private GameController _gameController;

    /// <summary>
    /// Oyunun ekran ile olan kisimlarini yoneten nesnedir.
    /// </summary>
    private TouchController touchController;

    /// <summary>
    /// secilen objelerin orta noktasindan donmesi icin start aninda bos bir sekilde olusturulan bir obje
    /// </summary>
    private GameObject _rotatePointObj;

    /// <summary>
    /// Oyunda sectigimiz hexagon gruplarinin etrafinda bulunan cerceve
    /// </summary>
    private GameObject _frameImage;

    /// <summary>
    /// Dusen birden fazla hexagonlari tutmak icin yazilmis bir class.
    /// Hexagonu ve nereye dusecegini tutuyor.
    /// </summary>
    private class FallingHexagons
    {
        public Vector3 targetPosition;
        public Hexagon hexagon;
    }
    #endregion


    private IEnumerator Start()
    {
        Init();
        LevelStartHexagonCreate();
        CamerePositionSet();
        AddCornerPoint();
        AddAllNeighbor();
        StartCoroutine(HexagonPlace());
        yield return new WaitForSeconds(1);
        CheckAvailableMove();
    }
    private void FixedUpdate()
    {
        switch (_gameController.GameStatusControl)
        {
            case GameStatus.Check_Again:
                ThereAreBreak();
                break;
        }
    }

    /// <summary>
    /// Donme isleminin gerceklestigi metot. Icerisinde ayrintili bir aciklamasi mevcut.
    /// </summary>
    /// <param name="rotatePoint"></param>
    /// <param name="hexagonGroup"></param>
    /// <param name="direction"></param>
    /// <param name="numberOfRound"></param>
    public void RotateGroup(Vector3 rotatePoint, Hexagon[] hexagonGroup, string direction, ref int numberOfRound)
    {
        Hexagon[] hex = null;
        int refNumberOfRound = numberOfRound;

        // rotation birden fazla kere ic ice cagrilir. Cunku donme islemi bir kere yapilmaz her bir rotation olayinda 3 kere art arda doner. 
        // numberOfRound ref olarak aliyorum cunku iceride degistirdigim bir int degeri boylece ic ice cagirsamda global olmayan bir  degeri istedigim gibi
        // manipule edebildim. Burada ilk defa metot cagrildiginda refNumberOfRound degeri sifir oluyor.
        // burada donecek objeleri bir orta noktada bulunan gameobjecte tasiyip hazir hale getiriyoruz.
        // else durumunda ise artik bizim grubumuz bir kere rotation kesin yapmis oluyor 2. ve 3. rotation olayinda tiklama olayi tekrardan olmus gibi yeni bir
        // 3 lu grup olusturuyorum. Cunku metodun ilerisinde (check metodunun icerisinde ) grubumun ilk elemani ile onun komsularina bakiyorum kirilma var mi diye
        // 2. rotation da bu 3 lu grubu degistirmeksek ilk elaman 1. grupta donen eleman oluyor boylece onun komsularina bakarak kontrol yapiyoruz ve bug olusuyor.
        // Kisaca 1. dondurmede kirlma olmaz ise 2. dondurmede yerler degismis ama 3.lu gruptaki yerler ayni kalmis oloyor bunu onlemek icin grubu yeniden bulduruyorum
        // Orasida else durumu.
        if (refNumberOfRound == 0)
        {
            hex = hexagonGroup;

            _rotatePointObj.transform.position = rotatePoint;
            for (int i = 0; i < hex.Length; i++)
            {
                hex[i].HexagonObj.transform.SetParent(_rotatePointObj.transform);
            }

            _gameController.MoveCount++;
        }
        else
        {
            hex = touchController.SelectGroup();
        }
        // saga mi sola mi donecek
        int rightLeftControl = RightLeftControl(direction);
        // donme olmadan once grubumun start pozisyonu
        Vector3[] hexagonsGroupStartPoints = HexagonGroupPositions(hex);

        foreach (var item in hex)
        {
            item.SpriteRenderer.sortingOrder = 1;
        }

        // donme islemi ve bitis olayi
        Debug.Log(120 * rightLeftControl * (refNumberOfRound + 1));
        _frameImage.transform.DORotate(new Vector3(0, 0, 120 * rightLeftControl * (refNumberOfRound + 1)), 0.2f);
        _rotatePointObj.transform.DORotate(new Vector3(0, 0, 120 * rightLeftControl * (refNumberOfRound + 1)), 0.2f).OnComplete(() =>
        {
            // birden fazla donme islemi olacagi icin counter i bir artiriyoruz.
            refNumberOfRound++;
            if (refNumberOfRound < 3)
            {
                // Grubumun son pozisyonu
                Vector3[] hexagonsGroupLastPoints = HexagonGroupPositions(hex);
                // Kontrol
                if (Check(hexagonsGroupStartPoints, hexagonsGroupLastPoints, hex))
                {
                    CheckBombCounter();
                    foreach (var item in hex)
                    {
                        item.SpriteRenderer.sortingOrder = 0;
                    }

                    for (int i = 0; i < hex.Length; i++)
                    {
                        _rotatePointObj.transform.GetChild(0).SetParent(transform);
                    }
                    _rotatePointObj.transform.eulerAngles = Vector3.zero;
                    _frameImage.transform.eulerAngles = Vector3.zero;
                    _gameController.GameStatusControl = GameStatus.Check_Again;

                }
                else
                {
                    RotateGroup(rotatePoint, hex, direction, ref refNumberOfRound);
                }
            }
            else
            {
                Vector3[] hexagonsGroupLastPoints = HexagonGroupPositions(hex);
                Check(hexagonsGroupStartPoints, hexagonsGroupLastPoints, hex);

                for (int i = 0; i < hex.Length; i++)
                {
                    _rotatePointObj.transform.GetChild(0).SetParent(transform);
                }

                _rotatePointObj.transform.eulerAngles = Vector3.zero;
                _frameImage.transform.eulerAngles = Vector3.zero;
                _gameController.GameStatusControl = GameStatus.Game_Stay;
                foreach (var item in hex)
                {
                    item.SpriteRenderer.sortingOrder = 0;
                }
            }
        });
    }

    /// <summary>
    /// her dondurme isleminden sonra cagrilir.Kirilacak obje var mi diye kontrol yapar.
    /// </summary>
    /// <param name="hexagonGroupStartPoints"> Grubun start pozsiyonu </param>
    /// <param name="hexagonGroupLastPoints">grubun son pozsiyonu </param>
    /// <param name="hexagons">Gruptaki elemanlar</param>
    /// <returns> true ise kirilma var false ise yok bir sonraki donme islemine gecebilir </returns>
    private bool Check(Vector3[] hexagonGroupStartPoints, Vector3[] hexagonGroupLastPoints, Hexagon[] hexagons)
    {
        // Check metodu dondurme isleminden sonra kirilan bir altigen var mı ona bakar. 
        // 3 boyutlu bir dizi olusturmamizin sebebi bir grupta 3 adet hexagon olabilir.
        // Burada gruptan kasit sudur. Ekrana tiklanildiginda secilen 3 lu hexagon grubu.
        Hexagon[] newOrganizeHexagonArray = new Hexagon[3];
        for (int i = 0; i < hexagonGroupStartPoints.Length; i++)
        {
            // ilk pozisyonlarindan son pozisyonlari cikarilir ve normali bulunur. Boylece yonunu bulmus oluruz.
            Vector3 normalizedPosition = (hexagonGroupLastPoints[i] - hexagonGroupStartPoints[i]).normalized;

            // parametreden gelen hexagonun hangi satirda ve hangi sutunda sayisini aliriz.
            int row = hexagons[i].Row;
            int column = hexagons[i].Column;

            // virgulden sonra 2 basamakli hale yuvarlamak icin.
            // bunun amaci sifira esitlik durumlarinda sifira esit olmamasiydi
            // 0.00023 gibi bir deger cikiyordu
            float x = Mathf.Round(normalizedPosition.x * 100f) / 100f;
            float y = Mathf.Round(normalizedPosition.y * 100f) / 100f;

            // bir dondurme isleminden sonra dizimin elemani hangi yone gittigine gore hangi komsusuna gittigini buluyoruz.
            // bulduktan sonra dizideki yerlerini degistiriyoruz.

            //rightDown hexagona gitmek icin
            if (x > 0 && y < 0)
            {
                newOrganizeHexagonArray[i] = column % 2 == 0 ?
                    OrganizeHexagon(row, column + 1, hexagons, i) : OrganizeHexagon(row - 1, column + 1, hexagons, i);
            }

            //leftUp hexagona gitmek icin
            else if (x < 0 && y > 0)
            {
                newOrganizeHexagonArray[i] = column % 2 == 0 ?
                    OrganizeHexagon(row + 1, column - 1, hexagons, i) : OrganizeHexagon(row, column - 1, hexagons, i);
            }

            //leftUp hexagona gitmek icin
            else if (x > 0 && y > 0)
            {
                newOrganizeHexagonArray[i] = column % 2 == 0 ?
                    OrganizeHexagon(row + 1, column + 1, hexagons, i) : OrganizeHexagon(row, column + 1, hexagons, i);
            }

            //leftdown hexagona gitmek icin
            else if (x < 0 && y < 0)
            {
                newOrganizeHexagonArray[i] = column % 2 == 0 ?
                    OrganizeHexagon(row, column - 1, hexagons, i) : OrganizeHexagon(row - 1, column - 1, hexagons, i);
            }

            //Up hexagona gitmek icin
            else if (x == 0 && y > 0)
            {
                newOrganizeHexagonArray[i] = OrganizeHexagon(row + 1, column, hexagons, i);
            }

            //Down hexagona gitmek icin
            else if (x == 0 && y < 0)
            {
                newOrganizeHexagonArray[i] = OrganizeHexagon(row - 1, column, hexagons, i);
            }
        }

        // Komsulari duzenle
        AddAllNeighbor();

        // Yok edilecek objeler var mi kontrolu.
        List<Hexagon> mylist = HexagonGroupNeighborColorCheck(newOrganizeHexagonArray);
        if (mylist.Count == 0)
        {
            return false;
        }
        else
        {
            BreakHexagon(mylist);
            return true;
        }
    }

    /// <summary>
    /// Kirilan objeleler olduktan sonra komsularinin yerine gecemesi ve yeni elemanlar eklenmesi icin
    /// </summary>
    private void ThereAreBreak()
    {
        // cerceveyi aciyoruz ve durumunu degistiriyoruz.
        _frameImage.SetActive(false);
        _gameController.GameStatusControl = GameStatus.There_Are_Break;

        //Simdi yok olma islemi gerceklesti ve bu metot cagirdi kirilan objeler var biliyoruz.
        // simdi kirilan objelerin ust komsulari asagi dusmesi lazim bu yuzden bir liste tanimladim
        // burada gidecegi pozisyonlari tutuyorum buradaki amacim butun islemler bittikten sonra -
        // gidilecek yere animasyonlu calistiriyorum.
        List<FallingHexagons> fallingHexagons = new List<FallingHexagons>();

        // Ana dizimi taramaya basladim burada alt nesneleri nul olan objeler buluyorum cunku asagi dusecekler
        for (int row = 1; row < _hexagon.GetLength(0); row++)
        {
            for (int column = 0; column < _hexagon.GetLength(1); column++)
            {
                // Kendim null degilsem ama altim null ise buraya bir kaydirma yapicalack
                if (_hexagon[row, column] != null && _hexagon[row, column].Down == null)
                {
                    // Burada kac zemin bosluk altim onu buluyorum
                    int emptyCount = HowManyFloorAreEmpty(_hexagon[row, column]);

                    // Asagi tarafa swap islemi
                    Hexagon temp = _hexagon[row, column];
                    _hexagon[row, column] = null;
                    _hexagon[row - emptyCount, column] = temp;

                    // griddeki pozisyonumu aldim.Cunku artik pozisyonum degisiyor kirilan objenin yerine gececegim
                    Vector3 targetPosition = grid.CellToWorld(new Vector3Int(row - emptyCount, column + 1, 0));

                    //dusecek objeyi listeme ekledim
                    fallingHexagons.Add(new FallingHexagons()
                    { hexagon = _hexagon[row - emptyCount, column], targetPosition = targetPosition });

                    // yeni yerimin ozelliklerini aliyorum. Satiri, sutunu, yeni ismi, pozisyonu gibi.
                    _hexagon[row - emptyCount, column].Row = row - emptyCount;
                    _hexagon[row - emptyCount, column].Column = column;
                    _hexagon[row - emptyCount, column].HexagonObj.name = $"{row - emptyCount}-{column}";
                    _hexagon[row - emptyCount, column].MyPos = targetPosition;
                    _hexagon[row - emptyCount, column].MyChilderPos = _corners[row - emptyCount, column].CornerPositions;

                    // Tum komsulari duzenletiyorum
                    AddAllNeighbor();

                    // diziyi bastan baslatiyorum.
                    // simdi diziyi burada baslatmak çok optimize bir yontem olmayabilir.
                    //Sonucta her dusen objeden sonra bir daha tarama yapiyor.
                    // Ama yok edilen objeler birden fazla ise ve aralarinda bosluk var ise garanti bir yontem oluyor.
                    // stres testine soktum ve fps de bir dususun olmadigini gordum 
                    // zaten bu islemi oyun calisir durumda surekli yapmiyor.
                    // burasi duzenlenebilir genede 
                    column = 0;
                    row = 1;
                }
            }
        }
        // objeler asagi dusmesi icin ve gerekli duzenlemelerin yapilmasi icin cagrilan metot.
        StartCoroutine(FixHexagonGrid(fallingHexagons));
    }

    /// <summary>
    /// Asagi tasima islemleri ve olusan bosluklari yeniden doldurma islemleri
    /// </summary>
    /// <param name="fallingHexagons"> asagi dusecek objler</param>
    /// <returns></returns>
    private IEnumerator FixHexagonGrid(List<FallingHexagons> fallingHexagons)
    {
        // DoMove ile asagi cekiyoruz
        for (int i = 0; i < fallingHexagons.Count; i++)
        {
            fallingHexagons[i].hexagon.HexagonObj.transform.DOMove(fallingHexagons[i].targetPosition, 0.1f);
            yield return new WaitForSeconds(0.035f);
        }

        // Asagi dusme islemi bitti ve yeni hexagonlar olusturulacak
        // Burasi bir metot haline getirilecek
        List<FallingHexagons> newHexagonsList = new List<FallingHexagons>();
        for (int row = 0; row < _hexagon.GetLength(0); row++)
        {
            for (int column = 0; column < _hexagon.GetLength(1); column++)
            {
                if (_hexagon[row, column] == null)
                {
                    Hexagon hex = InstantiateHexagon(row, column);
                    _hexagon[row, column] = hex;
                    newHexagonsList.Add(new FallingHexagons() { hexagon = hex, targetPosition = hex.MyPos });
                    AddAllNeighbor();
                }
            }
        }

        // Yeni olusan objeleride asagi dusurme islemi
        for (int i = 0; i < newHexagonsList.Count; i++)
        {
            if (i == newHexagonsList.Count - 1)
            {
                newHexagonsList[i].hexagon.HexagonObj.transform.DOMove(newHexagonsList[i].targetPosition, 0.1f).OnComplete(() =>
                {
                    // Son dusen elemandan sonra tum diziye bakiyoruz gene yok olacak objeler var mi diye.
                    CheckAll();
                });
            }
            else
            {
                newHexagonsList[i].hexagon.HexagonObj.transform.DOMove(newHexagonsList[i].targetPosition, 0.1f);
            }
            yield return new WaitForSeconds(0.035f);
        }
    }

    /// <summary>
    /// Tum diziyi kontrol eder kirilacak bir obje var mi diye.
    /// Burada amac yok edilen objelerin yerini yeni objeler geldi. Random gelen objelerden yok olacak var mi ona bakmak.
    /// </summary>
    private void CheckAll()
    {
        // kirilacak obje var mi yok mu bakar
        List<Hexagon> allList = new List<Hexagon>();

        // baslangicta true deger dondurur eger kirilacak objeler varsa kirdiktan sonra tum kontrol islemlerini bir daha yapmasi gerekir.
        // bu yuzden checkControl = false olur;
        bool checkControl = true;
        for (int row = 0; row < _hexagon.GetLength(0); row++)
        {
            for (int column = 0; column < _hexagon.GetLength(1); column++)
            {
                Hexagon[] hex = new Hexagon[] { _hexagon[row, column] };
                List<Hexagon> mylist = HexagonGroupNeighborColorCheck(hex);
                foreach (var item in mylist)
                {
                    checkControl = false;
                    allList.Add(item);
                }
            }
        }

        // Hexagonlari kir.
        BreakHexagon(allList);

        // check kontrolune gore oyunun durumunu degistir.
        if (!checkControl)
        {
            // Bu duruma surekli bakan FixedUpdate metodu vardir.
            _gameController.GameStatusControl = GameStatus.Check_Again;
        }
        else
        {
            touchController._selectedObj = false;
            _gameController.GameStatusControl = GameStatus.Game_Stay;
            CheckAvailableMove();
        }
    }

    /// <summary>
    /// Hamle var mi kontolu bu metotta gerceklesir.
    /// Basit bir mantik ile calisir. 
    /// 1 - Bir hexagon up komsu ile ayni mi rightDown ile ayni mi ya da right up ile ayni mi diye bakar.
    /// 2 - Eger kosullardan biri saglaniyorsa demekki 3 lu bir grup olsa hamle var demek olur. 
    ///  Grubun ucuncu elemani icin ise ucuncu elemanin gelecegi yerin tum komsularina bakilir.
    /// 3- Tum komsularinin icerisinde kendisi ve kendisi ile ayni renkte olan diger komsu da bulunur.
    ///  Bu yuzden ayni renkte olan hexagon sayisi 3 ten buyuk esitse hamle var demektir. 
    /// Not: Bunun testini satir ve sutun sayisini 3 e 3 luk ayarlarsaniz ve renk sayisini artirirsaniz oyunu basinda
    /// // bile hamle olma ihtimali cok dusuyor. Bu yuzden game over yazisi cikiyor. 
    /// </summary>
    private void CheckAvailableMove()
    {
        foreach (var item in _hexagon)
        {
            if (item.RightDown != null && item.ColorIndex == item.RightDown.ColorIndex)
            {
                if (item.RightUp != null && CheckAvailableMoveForOnePoint(item.RightUp, item))
                {
                    goto Continue;
                }
                if (item.Down != null && CheckAvailableMoveForOnePoint(item.Down, item))
                {
                    goto Continue;
                }
            }
            if (item.Up != null && item.ColorIndex == item.Up.ColorIndex)
            {
                if (item.RightUp != null && CheckAvailableMoveForOnePoint(item.RightUp, item))
                {
                    goto Continue;
                }
                if (item.LeftUp != null && CheckAvailableMoveForOnePoint(item.LeftUp, item))
                {
                    goto Continue;
                }
            }
            if (item.RightUp != null && item.ColorIndex == item.RightUp.ColorIndex)
            {
                if (item.Up != null && CheckAvailableMoveForOnePoint(item.Up, item))
                {
                    goto Continue;
                }
                if (item.RightDown != null && CheckAvailableMoveForOnePoint(item.RightDown, item))
                {
                    goto Continue;
                }
            }
        }
        _gameController.GameStatusControl = GameStatus.Game_Over;

    Continue: { }
    }
}

public partial class HexagonController // baslangic metotlari icerir
{
    #region Baslangic Icin Metotlar

    private void Init()
    {
        _hexagon = new Hexagon[_hexagonProperty.row, _hexagonProperty.column];
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        touchController = GameObject.FindGameObjectWithTag("TouchTag").GetComponent<TouchController>();
        _rotatePointObj = new GameObject("RotatePoint");
        _frameImage = GameObject.Find("FrameImage");
    }
    /// <summary>
    /// Gridin satir sutun sayisina gore kameranin konumunu ayarlar.
    /// </summary>
    private void CamerePositionSet()
    {
        float x = 0;
        x = _hexagon.GetLength(1) % 2 == 0 ? 0.3f : 0.6f;
        Vector3 cameraStartPos = grid.CellToWorld(new Vector3Int(_hexagon.GetLength(0) / 2, _hexagon.GetLength(1) / 2, 0));
        cameraStartPos.Set(cameraStartPos.x + x, cameraStartPos.y, Camera.main.transform.position.z);
        Camera.main.transform.position = cameraStartPos;
    }

    /// <summary>
    /// Her hexagonun icerisinde bulunan noktalari diziye atar.
    /// </summary>
    private void AddCornerPoint()
    {
        _corners = new Corners[_hexagon.GetLength(0), _hexagon.GetLength(1)];
        foreach (var item in _hexagon)
        {
            Vector3[] positionNode = new Vector3[6];
            for (int i = 0; i < positionNode.Length; i++)
            {
                positionNode[i] = item.HexagonObj.transform.GetChild(0).GetChild(i).position;
            }
            Corners corner = new Corners(positionNode);
            _corners[item.Row, item.Column] = corner;
            item.MyChilderPos = positionNode;
        }
    }

    /// <summary>
    /// Altigen olusturmak icin baslangic metodu.
    /// </summary>
    private void LevelStartHexagonCreate()
    {
        //Satir kadar for doner
        for (int row = 0; row < _hexagon.GetLength(0); row++)
        {
            // Sutun olusturmak icin LevelStartRowCreate metodunu cagiracak
            LevelStartRowCreate(row);
        }
    }

    /// <summary>
    /// Satir olusturmak icin
    /// </summary>
    /// <param name="row">hangi satir</param>
    private void LevelStartRowCreate(int row)
    {
        Hexagon hex = null;
        // Sutun kadar for doner
        for (int column = 0; column < _hexagon.GetLength(1); column++)
        {
            // Eger satirim 0 ise hic bir kontrol yapmadan altigenler olusturulur.
            // Cunku 0,1  - 0,2 - 0,3 .... giden dizimde renkler ayni gelse bile digerleri eklenmedigi icin random renk atayabiliriz.
            // Ama bir ust satira geicldiginde artik ayni renk var mi diye kontrol etmesi lazim.
            // Bunun sebebi oyun basladiginda birbirini goturecek komsularin olmamasi lazim.
            int colorIndex = 0;
            if (row == 0)
            {
                Color color = _hexagonProperty.RandomColor(out colorIndex);
                hex = new Hexagon(Instantiate(_hexagonPrefab), color, row, column, colorIndex);
                _hexagon[row, column] = hex;
                continue;
            }

            // if olmaz ise artik ayni renk mi degil mi kontrolu yapmasi lazim.
            Color hexColor = Color.clear;

            // Burada 3 grup belirledim bir altigenin ayni renk mi degil mi diye bakmasi icin su sekilde grupladim
            // ornegin 1 e 1 e bir obje geldi 
            // birinci grup sudur 1,1 , solust ve sol 
            // ikinci grup sudur 1,1 , solalt ve alt
            // ucuncu grup sufur 1,1 alt ve sagalt
            // peki diger gruplara neden bakmadik cunku daha olusturulmamis oluyorlar. 
            // GrupControlForNotTheSameColor bu metot cagrildiginda false donuyorsa demek ki ortak 3 renk var bir grupta.
            // true donerse farkli renk o zaman bir hexagon olusturabilirim.

            int colorIndex2 = 0;
            while (!GrupControlForNotTheSameColor(row, column, out hexColor, out colorIndex2)) ;

            hex = new Hexagon(Instantiate(_hexagonPrefab), hexColor, row, column, colorIndex2);
            _hexagon[row, column] = hex;
        }
    }

    /// <summary>
    /// Bir grupta 3 tane ayni renk var mi ona bakiyoruz.
    /// 3 Grup oldugu icin for 3 kere doner.
    /// </summary>
    /// <param name="row"> satir </param>
    /// <param name="column"> sutun </param>
    /// <param name="hexColor">gelen renk</param>
    /// <returns>Bir grupta 3 tane ayni renk var mi ? Varsa false yoksa true</returns>
    private bool GrupControlForNotTheSameColor(int row, int column, out Color hexColor, out int colorIndexReturn)
    {
        Color randomColor = _hexagonProperty.RandomColor(out int colorIndex);
        for (int whichGrup = 0; whichGrup < 3; whichGrup++)
        {
            // sutun 0 ise 0,0  0,1   0,2 .. diye gitse de onemli degil oraya random bir renk verilebilir.
            if (column == 0)
            {
                hexColor = randomColor;
                colorIndexReturn = colorIndex;
                return true;
            }
            if (whichGrup == 0)
            {
                // eger 2 ile bolumunden kalan 0 ise kosullar farkli
                if (column % 2 == 0)
                {
                    if (row != _hexagon.GetLength(0) - 1)
                    {
                        if (_hexagon[row + 1, column - 1] != null && _hexagon[row, column - 1] != null)
                        {
                            if (randomColor == _hexagon[row + 1, column - 1].ColorObj && randomColor == _hexagon[row, column - 1].ColorObj)
                            {
                                hexColor = Color.clear;
                                colorIndexReturn = 100000;
                                return false;
                            }
                        }
                    }
                }
                // eger else ise kosullar farkli olacak
                else
                {
                    if (_hexagon[row, column - 1] != null && _hexagon[row - 1, column - 1] != null)
                    {
                        if (randomColor == _hexagon[row, column - 1].ColorObj && randomColor == _hexagon[row - 1, column - 1].ColorObj)
                        {
                            hexColor = Color.clear;
                            colorIndexReturn = 100000;
                            return false;
                        }
                    }
                }
            }
            else if (whichGrup == 1)
            {
                if (column % 2 == 0)
                {
                    if (_hexagon[row, column - 1] != null && _hexagon[row, column - 1] != null)
                    {
                        if (randomColor == _hexagon[row, column - 1].ColorObj && randomColor == _hexagon[row, column - 1].ColorObj)
                        {
                            hexColor = Color.clear;
                            colorIndexReturn = 100000;
                            return false;
                        }
                    }
                }
                else
                {
                    if (_hexagon[row - 1, column - 1] != null && _hexagon[row - 1, column] != null)
                    {
                        if (randomColor == _hexagon[row - 1, column - 1].ColorObj && randomColor == _hexagon[row - 1, column].ColorObj)
                        {
                            hexColor = Color.clear;
                            colorIndexReturn = 100000;
                            return false;
                        }
                    }
                }
            }
            else if (whichGrup == 2)
            {
                if (column % 2 == 0)
                {
                    if (column != _hexagon.GetLength(1) - 1)
                    {
                        if (_hexagon[row - 1, column] != null && _hexagon[row, column + 1] != null)
                        {
                            if (randomColor == _hexagon[row - 1, column].ColorObj && randomColor == _hexagon[row - 1, column + 1].ColorObj)
                            {
                                hexColor = Color.clear;
                                colorIndexReturn = 100000;
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    if (column != _hexagon.GetLength(1) - 1)
                    {
                        if (_hexagon[row - 1, column] != null && _hexagon[row - 1, column + 1] != null)
                        {
                            if (randomColor == _hexagon[row - 1, column].ColorObj && randomColor == _hexagon[row - 1, column + 1].ColorObj)
                            {
                                hexColor = Color.clear;
                                colorIndexReturn = 100000;
                                return false;
                            }
                        }
                    }
                }
            }
        }
        hexColor = randomColor;
        colorIndexReturn = colorIndex;
        return true;
    }

    /// <summary>
    /// Hexagonlarin sahneye yerlestirilmesi ve hexagonlarin icerisindeki pointlerin kendi classlarina atama islemi
    /// </summary>
    /// <returns></returns>
    private IEnumerator HexagonPlace()
    {
        for (int row = 0; row < _hexagon.GetLength(0); row++)
        {
            for (int column = 0; column < _hexagon.GetLength(1); column++)
            {
                // dizimi sirasi ile geziyorum
                Hexagon hex = _hexagon[row, column];

                //gritteki pozisyonunu bulup classima atiyorum
                hex.MyPos = grid.CellToWorld(new Vector3Int(row, column + 1, 0));

                //Baslangic pozisyonumun x sini ayarliyorum boylece altigenler havadan gitmesi gerektigi yere gidecekler
                hex.HexagonObj.transform.position = new Vector3(grid.CellToWorld(new Vector3Int(row, column + 1, 0)).x, 15);

                //altigenlerimin koslerini ekletiyorum 
                hex.HexagonObj.transform.DOMove(grid.CellToWorld(new Vector3Int(row, column + 1, 0)), 0.2f * column).OnComplete(() =>
                {
                    AddNodesInTheHexagon(hex);
                    // Destroy(myPosChild.parent.gameObject);
                });

                // son elemana gelindiginde oyun baslasin diyorum
                if (row == _hexagon.GetLength(0) - 1 && column == _hexagon.GetLength(1) - 1)
                {
                    hex.HexagonObj.transform.DOMove(grid.CellToWorld(new Vector3Int(row, column + 1, 0)), 0.2f * column).OnComplete(() =>
                    {
                        _gameController.GameStatusControl = GameStatus.Game_Stay;
                    });
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
    }

    private void AddNodesInTheHexagon(Hexagon hex)
    {
        Transform myPosChild = null;
        for (int i = 0; i < 6; i++)
        {
            myPosChild = hex.HexagonObj.transform.Find("Pointler").GetChild(i);
            hex.MyChilderPos[i] = myPosChild.position;
        }
    }

    /// <summary>
    /// Butun altigenlerin komsularini bulmak icin bu metot kullaniliyor.
    /// </summary>
    private void AddAllNeighbor()
    {
        for (int row = 0; row < _hexagon.GetLength(0); row++)
        {
            for (int column = 0; column < _hexagon.GetLength(1); column++)
            {
                if (_hexagon[row, column] != null)
                {
                    _hexagon[row, column].Up =
                      NoArrayOverflow(row + 1, _hexagon.GetLength(0)) == true ? _hexagon[row + 1, column] : null;

                    _hexagon[row, column].Down =
                        NoArrayOverflow(row - 1, _hexagon.GetLength(0)) == true ? _hexagon[row - 1, column] : null;

                    if (column % 2 == 0)
                    {
                        _hexagon[row, column].RightDown =
                            NoArrayOverflow(column + 1, _hexagon.GetLength(1)) == true ? _hexagon[row, column + 1] : null;

                        _hexagon[row, column].RightUp =
                            NoArrayOverflow(row + 1, _hexagon.GetLength(0)) == true &&
                            NoArrayOverflow(column + 1, _hexagon.GetLength(1)) == true ? _hexagon[row + 1, column + 1] : null;

                        _hexagon[row, column].LeftDown =
                           NoArrayOverflow(column - 1, _hexagon.GetLength(1)) == true ? _hexagon[row, column - 1] : null;

                        _hexagon[row, column].LeftUp =
                           NoArrayOverflow(row + 1, _hexagon.GetLength(0)) == true &&
                           NoArrayOverflow(column - 1, _hexagon.GetLength(1)) == true ? _hexagon[row + 1, column - 1] : null;
                    }
                    else
                    {
                        _hexagon[row, column].RightDown =
                            NoArrayOverflow(row - 1, _hexagon.GetLength(0)) == true &&
                            NoArrayOverflow(column + 1, _hexagon.GetLength(1)) == true ? _hexagon[row - 1, column + 1] : null;

                        _hexagon[row, column].RightUp =
                            NoArrayOverflow(column + 1, _hexagon.GetLength(1)) == true ? _hexagon[row, column + 1] : null;

                        _hexagon[row, column].LeftDown =
                           NoArrayOverflow(column - 1, _hexagon.GetLength(1)) == true &&
                           NoArrayOverflow(row - 1, _hexagon.GetLength(0)) == true ? _hexagon[row - 1, column - 1] : null;

                        _hexagon[row, column].LeftUp =
                           NoArrayOverflow(column - 1, _hexagon.GetLength(1)) == true ? _hexagon[row, column - 1] : null;
                    }
                }

            }

        }
    }
    #endregion
}

public partial class HexagonController // Yardimci metotlar
{
    #region Yardimci Metotlar

    /// <summary>
    /// Altigenler komsularini bulurken bazi null olabilecek komsulari olabilir.
    /// Ornegin 0,0 dizisinde bulunan bir elemanin -1 -1 gibi bir komsusu yoktur.
    /// Bu tip durumlarda dizi hata verecegi icin bu metotta kontrol yaptirttim.
    /// </summary>
    /// <param name="index"></param>
    /// <param name="arrayLength"></param>
    /// <returns></returns>
    private bool NoArrayOverflow(int index, int arrayLength)
    {
        if (index < 0)
            return false;
        if (index == arrayLength)
            return false;
        return true;
    }

    /// <summary>
    /// Sagami donecek solami donecek bunu kontrol ediyor
    /// </summary>
    /// <param name="direction">Yon</param>
    /// <returns></returns>
    private int RightLeftControl(string direction)
    {
        if (direction == "right")
            return 1;
        else
            return -1;
    }

    /// <summary>
    /// Verilen hexagonlarin pozisyonlarini aliyor.
    /// </summary>
    /// <param name="hexagons"></param>
    /// <returns></returns>
    private Vector3[] HexagonGroupPositions(Hexagon[] hexagons)
    {
        return new Vector3[]
        {
            hexagons[0].HexagonObj.transform.position,
            hexagons[1].HexagonObj.transform.position,
            hexagons[2].HexagonObj.transform.position
        };
    }

    /// <summary>
    /// Ayni renktemi degil mi onu kontrol ediyor
    /// </summary>
    /// <param name="firstHex"></param>
    /// <param name="secondHex"></param>
    /// <returns></returns>
    private bool CheckHexColor(Hexagon firstHex, Hexagon secondHex)
    {
        if (secondHex != null && firstHex != null)
        {
            if (firstHex.ColorIndex == secondHex.ColorIndex)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    /// <summary>
    /// verilen noktadan sahneye dogru ray cizilir.
    /// </summary>
    /// <param name="position"> baslangic pozsiyonu</param>
    /// <returns></returns>
    public RaycastHit2D RayToScreen(Vector3 position)
    {
        return Physics2D.Raycast(Camera.main.ScreenToWorldPoint(position), Vector2.right);
    }

    /// <summary>
    /// Hexagonlari kirar
    /// </summary>
    /// <param name="hexList"> kirilacak hexxagonlar</param>
    private void BreakHexagon(List<Hexagon> hexList)
    {
        List<Hexagon> bombList = new List<Hexagon>();
        foreach (Hexagon item in hexList)
        {
            if (_hexagon[item.Row, item.Column] != null)
            {
                if (_hexagon[item.Row, item.Column].IsBomb)
                {
                    bombList.Add(_hexagon[item.Row, item.Column]);
                }
                _hexagon[item.Row, item.Column] = null;
                Destroy(item.HexagonObj);
                AddAllNeighbor();
            }
        }

        foreach (var item in _hexagon)
        {
            if (item == null)
            {
                _gameController.TotalScore += 5;
                _gameController.BombScoreCounter += 5;
            }
        }

        foreach (var bompList in bombList)
        {
            foreach (var hexagon in _hexagon)
            {
                if (hexagon != null && hexagon.ColorIndex == bompList.ColorIndex)
                {
                    _hexagon[hexagon.Row, hexagon.Column] = null;
                    Destroy(hexagon.HexagonObj);
                    _gameController.TotalScore += 5;
                    _gameController.BombScoreCounter += 5;
                }
            }
            AddAllNeighbor();
        }

    }

    /// <summary>
    /// Ayni renkten objler var mi diye bakar liste dondurur.
    /// </summary>
    /// <param name="newOrganizeHexagonArray"></param>
    /// <returns></returns>
    private List<Hexagon> HexagonGroupNeighborColorCheck(Hexagon[] newOrganizeHexagonArray)
    {
        List<Hexagon> mylist = new List<Hexagon>();
        for (int hexCounter = 0; hexCounter < newOrganizeHexagonArray.Length; hexCounter++)
        {
            // Her hexagonun 6 noktasi oldugu icin 
            for (int cornerCounter = 0; cornerCounter < 6; cornerCounter++)
            {
                if (newOrganizeHexagonArray[hexCounter] != null)
                {
                    if (cornerCounter == 0)
                    {
                        if (CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].Up) &&
                            CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].LeftUp))
                        {
                            mylist.Add(newOrganizeHexagonArray[hexCounter]);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].Up);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].LeftUp);
                        }
                    }
                    else if (cornerCounter == 1)
                    {
                        if (CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].Up) &&
                            CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].RightUp))
                        {
                            mylist.Add(newOrganizeHexagonArray[hexCounter]);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].Up);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].RightUp);
                        }
                    }
                    else if (cornerCounter == 2)
                    {
                        if (CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].RightUp) &&
                            CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].RightDown))
                        {
                            mylist.Add(newOrganizeHexagonArray[hexCounter]);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].RightUp);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].RightDown);
                        }
                    }
                    else if (cornerCounter == 3)
                    {
                        if (CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].RightDown) &&
                            CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].Down))
                        {
                            mylist.Add(newOrganizeHexagonArray[hexCounter]);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].RightDown);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].Down);
                        }
                    }
                    else if (cornerCounter == 4)
                    {
                        if (CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].LeftDown) &&
                            CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].Down))
                        {
                            mylist.Add(newOrganizeHexagonArray[hexCounter]);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].LeftDown);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].Down);
                        }
                    }
                    else if (cornerCounter == 5)
                    {
                        if (CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].LeftDown) &&
                            CheckHexColor(newOrganizeHexagonArray[hexCounter], newOrganizeHexagonArray[hexCounter].LeftUp))
                        {
                            mylist.Add(newOrganizeHexagonArray[hexCounter]);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].LeftDown);
                            mylist.Add(newOrganizeHexagonArray[hexCounter].LeftUp);
                        }
                    }
                }
            }
        }
        return mylist;
    }

    /// <summary>
    /// ana dizim (_hexagon) burada elemanlar degisecek. Cunku rotetion gerceklestiginde elemanlarin yerleri degisiyor
    /// bu metot bu ise yariyor. Verilen row ve column degerlerine gore diziyi degistiriyor.
    /// </summary>
    /// <param name="row"></param>
    /// <param name="column"></param>
    /// <param name="hexagons"></param>
    /// <param name="i">hexagon grubumun hangi elemani </param>
    /// <returns></returns>
    private Hexagon OrganizeHexagon(int row, int column, Hexagon[] hexagons, int i)
    {
        if (NoArrayOverflow(row, _hexagon.GetLength(0)) && NoArrayOverflow(column, _hexagon.GetLength(1)))
        {
            Vector3 myPos = _hexagon[row, column].MyPos;
            Vector3[] childPosition = _hexagon[row, column].MyChilderPos;

            _hexagon[row, column] =
                new Hexagon(hexagons[i].HexagonObj, hexagons[i].ColorObj, row, column, childPosition, myPos, hexagons[i].ColorIndex);

            if (hexagons[i].IsBomb)
            {
                _hexagon[row, column].IsBomb = hexagons[i].IsBomb;
                _hexagon[row, column].HexagonBomb = hexagons[i].HexagonBomb;
            }
            return _hexagon[row, column];
        }
        return null;

    }

    /// <summary>
    /// En yakin kose noktasini hesaplar
    /// </summary>
    /// <param name="touchedHexagon"></param>
    /// <param name="touchPosition"></param>
    /// <returns></returns>
    public string FindNearestCorner(Hexagon touchedHexagon, Vector3 touchPosition)
    {
        float distance = float.MaxValue;
        int cornerName = 0;

        for (int i = 0; i < 6; i++)
        {
            if (i == 0 && (touchedHexagon.Up == null || touchedHexagon.LeftUp == null))
                continue;
            if (i == 1 && (touchedHexagon.Up == null || touchedHexagon.RightUp == null))
                continue;
            if (i == 2 && (touchedHexagon.RightUp == null || touchedHexagon.RightDown == null))
                continue;
            if (i == 3 && (touchedHexagon.RightDown == null || touchedHexagon.Down == null))
                continue;
            if (i == 4 && (touchedHexagon.Down == null || touchedHexagon.LeftDown == null))
                continue;
            if (i == 5 && (touchedHexagon.LeftDown == null || touchedHexagon.LeftUp == null))
                continue;

            float tempDistance = Vector3.Distance(touchPosition, touchedHexagon.MyChilderPos[i]);

            if (tempDistance < distance)
            {
                distance = tempDistance;
                cornerName = i;
            }
        }
        return cornerName.ToString();
    }

    /// <summary>
    /// Hexagon grup olusturma
    /// </summary>
    /// <param name="cornerName"></param>
    /// <param name="hexagon"></param>
    /// <returns></returns>
    public Hexagon[] FindHexagonGrup(string cornerName, Hexagon hexagon)
    {
        if (cornerName == "0")
        {
            return new Hexagon[] { hexagon, hexagon.Up, hexagon.LeftUp };
        }
        if (cornerName == "1")
        {
            return new Hexagon[] { hexagon, hexagon.Up, hexagon.RightUp };
        }
        if (cornerName == "2")
        {
            return new Hexagon[] { hexagon, hexagon.RightUp, hexagon.RightDown };
        }
        if (cornerName == "3")
        {
            return new Hexagon[] { hexagon, hexagon.RightDown, hexagon.Down };
        }
        if (cornerName == "4")
        {
            return new Hexagon[] { hexagon, hexagon.Down, hexagon.LeftDown };
        }
        if (cornerName == "5")
        {
            return new Hexagon[] { hexagon, hexagon.LeftDown, hexagon.LeftUp };
        }
        return null;
    }

    private void CheckBombCounter()
    {
        foreach (var item in _hexagon)
        {
            if (item != null && item.IsBomb)
            {
                item.HexagonBomb._bompCounter -= 1;
            }
        }
    }

    private Hexagon InstantiateHexagon(int row, int column)
    {
        Hexagon hex = null;
        GameObject hexagonObject = null;
        Color color;
        int colorIndex = 0;

        if (_gameController.BombScoreCounter > 1000f) // public olarak duzenlenmesi gerek
        {
            hexagonObject = Instantiate(_hexagonPrefabBomb, new Vector2(grid.CellToWorld(new Vector3Int(row, column + 1, 0)).x, 15), Quaternion.identity);
            color = _hexagonProperty.RandomColor(out colorIndex);
            hex = new Hexagon(hexagonObject, color, row, column, colorIndex);
            hex.IsBomb = true;
            hex.HexagonBomb = hex.HexagonObj.GetComponent<HexagonBomb>();
            _gameController.BombScoreCounter = 0;
        }
        else
        {
            hexagonObject = Instantiate(_hexagonPrefab, new Vector2(grid.CellToWorld(new Vector3Int(row, column + 1, 0)).x, 15), Quaternion.identity);
            color = _hexagonProperty.RandomColor(out colorIndex);
            hex = new Hexagon(hexagonObject, color, row, column, colorIndex);
        }

        hex.MyPos = grid.CellToWorld(new Vector3Int(row, column + 1, 0));
        hex.MyChilderPos = _corners[row, column].CornerPositions;

        return hex;
    }

    private int HowManyFloorAreEmpty(Hexagon hexagon)
    {
        int counter = 0;
        int row = hexagon.Row;
        int column = hexagon.Column;

        while (row > 0 && _hexagon[row - 1, column] == null)
        {
            row -= 1;
            counter++;
        }
        return counter;
    }

    private bool CheckAvailableMoveForOnePoint(Hexagon neighborItem, Hexagon item)
    {
        int counter = 0;
        if (neighborItem.Up != null && neighborItem.Up.ColorIndex == item.ColorIndex)
            counter++;
        if (neighborItem.Down != null && neighborItem.Down.ColorIndex == item.ColorIndex)
            counter++;
        if (neighborItem.RightUp != null && neighborItem.RightUp.ColorIndex == item.ColorIndex)
            counter++;
        if (neighborItem.RightDown != null && neighborItem.RightDown.ColorIndex == item.ColorIndex)
            counter++;
        if (neighborItem.LeftUp != null && neighborItem.LeftUp.ColorIndex == item.ColorIndex)
            counter++;
        if (neighborItem.LeftDown != null && neighborItem.LeftDown.ColorIndex == item.ColorIndex)
            counter++;
        Debug.Log("true _----  " + counter);
        return counter >= 3 ? true : false;

    }
    #endregion
}
