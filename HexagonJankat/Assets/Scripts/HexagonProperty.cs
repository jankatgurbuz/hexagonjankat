﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[CreateAssetMenu(fileName = "HexagonProperty", menuName = "ScriptableObjects/HexagonProperty", order = 1)]
public class HexagonProperty : ScriptableObject
{
    /// <summary>
    /// maximum renk sayisi
    /// </summary>
    [Range(3, 7)] public int ColorLength = 5;

    /// <summary>
    /// Hexagon renkleri
    /// </summary>
    public Color[] colors;

    /// <summary>
    /// Satir Sayisi
    /// </summary>
    public int row = 8;

    /// <summary>
    /// Sutun Sayisi
    /// </summary>
    public int column = 9;
    
    /// <summary>
    /// Random uretilecek renk ve indis degeri.
    /// </summary>
    /// <param name="randomColorIndex"></param>
    /// <returns></returns>
    public Color RandomColor(out int randomColorIndex)
    {
        int random = Random.Range(0,ColorLength);
        randomColorIndex = random;
        return colors[random];
    }
}

