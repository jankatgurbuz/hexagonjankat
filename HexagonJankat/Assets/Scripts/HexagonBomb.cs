﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HexagonBomb : MonoBehaviour
{
    /// <summary>
    /// oyunu yoneten nesne
    /// </summary>
    private GameController _gameController;

    /// <summary>
    /// oyunu yoneten objenin transformu
    /// </summary>
    private Transform _gameControllerTransform;

    /// <summary>
    /// Kendi kanvasi. Bombanin sayicisini guncelleyecegim
    /// </summary>
    private Transform _canvas;

    /// UI
    private Image _bombImage;
    private Text _bombCounter;
    ///-----
    
    private SpriteRenderer spRenderer;
    [HideInInspector] public int _bompCounter;
    private void Start()
    {
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        _gameControllerTransform = _gameController.transform;
        _canvas = transform.Find("Canvas");
        _bombCounter = _canvas.Find("Counter").GetComponent<Text>();
        _bombImage = _canvas.Find("BombImage").GetComponent<Image>();
        spRenderer = GetComponent<SpriteRenderer>();
        _bompCounter = Random.Range(3,5);
    }

    private void FixedUpdate()
    {
        _canvas.rotation = _gameControllerTransform.rotation;
        _bombImage.color = spRenderer.color;
        _bombCounter.text = _bompCounter.ToString();
        if (_bompCounter == 0)
        {
            _gameController.GameStatusControl = GameStatus.Game_Over;
        }
    }
}
