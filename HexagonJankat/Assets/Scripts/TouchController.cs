﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;


public partial class TouchController : MonoBehaviour
{
    /// <summary>
    /// bir hexagon secildi mi secilmedi mi ? 
    /// </summary>
    [HideInInspector] public bool _selectedObj = false;

    /// <summary>
    /// _hexagonController controlleri yonetmek icin 
    /// </summary>
    private HexagonController _hexagonController;

    /// <summary>
    /// Oyunun yonetici nesnesi
    /// </summary>
    private GameController _gameController;

    /// <summary>
    /// ilk ekran pozisyonu
    /// </summary>
    private Vector2 _downPos;

    /// <summary>
    /// secilen objenin grubunun orta noktasi
    /// </summary>
    private Vector3 selectedPoint = Vector3.zero;

    /// <summary>
    /// gruplarin cercevesinin yonunu soyleyen degisken
    /// </summary>
    private string _frameImageDirection = string.Empty;

    /// <summary>
    /// 3 lu grubun dizisi(secilen objenin grubunun) 
    /// </summary>
    private Hexagon[] _hexagonGroup;

    /// <summary>
    /// secim yaptiktan sonra elimizin pozisyonu
    /// </summary>
    private Vector3 touchPos = Vector3.zero;

    /// <summary>
    /// Oyunda gruplarin etrafindaki cerceve objesi
    /// </summary>
    private GameObject _frameImage;

    
    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.1f);
        Init();
        SetupLeanTouchMetot();
    }
    private void Init()
    {
        _hexagonController = GameObject.FindGameObjectWithTag("HexagonControllerTag").GetComponent<HexagonController>();
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        _frameImage = GameObject.Find("FrameImage");
        _frameImage.SetActive(false);
    }
    private void SetupLeanTouchMetot()
    {
        LeanTouch.OnFingerUp += OnFingerUp;
        LeanTouch.OnFingerDown += OnFingerDown;
        LeanTouch.OnFingerSet += OnFingerSet;
    }
    private void OnFingerDown(LeanFinger obj)
    {
        if (_gameController.GameStatusControl == GameStatus.Game_Stay)
        {
            _downPos = obj.ScreenPosition;
        }
    }
    /// <summary>
    /// Objeyi dondurme islemi bu metotta gerceklesiyor. HexagonController icerisindeki RotateGroup cagrilir.
    /// </summary>
    /// <param name="obj"></param>
    private void OnFingerSet(LeanFinger obj)
    {
        if (_gameController.GameStatusControl == GameStatus.Game_Stay)
        {
            if (Vector3.Distance(_downPos, obj.ScreenPosition) > 45 && _selectedObj)
            {
                _gameController.GameStatusControl = GameStatus.Rotate;
                string direction = WhichDirectionRotate((_downPos - obj.ScreenPosition).normalized);

                // buradaki counteri ref olarak gonderiyoruz. Boylece 3 tur atmasi icin RotateGroup kendini bir daha cagiriyor
                // her turdan sonra refi bir artiriyoruz.
                int counter = 0;
                _hexagonController.RotateGroup(selectedPoint, _hexagonGroup, direction, ref counter);
            }
        }
    }
    private void OnFingerUp(LeanFinger obj)
    {
        // secili degilse sec
        if (!_selectedObj)
        {
            Selected(obj);
        }

        // eli cok az kaydirdiysa gene sec
        else if (Vector3.Distance(_downPos, obj.ScreenPosition) < 45)
        {
            Selected(obj);
        }
    }
    // secme islemleri
    private void Selected(LeanFinger obj)
    {
        if (_gameController.GameStatusControl == GameStatus.Game_Stay)
        {
            Vector3 tempPos = SelectGroup(obj.ScreenPosition);
            if (tempPos != Vector3.zero)
            {
                selectedPoint = SelectGroup(obj.ScreenPosition);
                touchPos = obj.ScreenPosition;
                _selectedObj = true;

                if (_frameImageDirection == "R")
                {
                    _frameImage.transform.localScale = new Vector3(0.39f, 0.39f, 0.39f);
                }
                else if (_frameImageDirection == "L")
                {
                    _frameImage.transform.localScale = new Vector3(-0.39f, 0.39f, 0.39f);

                }
                _frameImage.transform.position = selectedPoint;
                _frameImage.SetActive(true);

            }
        }
    }
}

public partial class TouchController // Yardimci metotlar
{
    private RaycastHit2D RayToScreen(Vector3 pos)
    {
        return Physics2D.Raycast(Camera.main.ScreenToWorldPoint(pos), Vector2.right);
    }
    // secilen gurubu buluyor ve orta noktasini donduruyor
    private Vector3 SelectGroup(Vector3 pos)
    {
        RaycastHit2D hit = RayToScreen(pos);
        Debug.DrawLine(Camera.main.ScreenToWorldPoint(pos), hit.point, Color.red, 10);

        if (hit.collider != null)
        {
            if (hit.collider.tag == "HexagonTag")
            {
                string[] splitRowAndColumn = hit.collider.name.Split('-');
                Hexagon hexagon = _hexagonController._hexagon[int.Parse(splitRowAndColumn[0]), int.Parse(splitRowAndColumn[1])];
                string cornerName = _hexagonController.FindNearestCorner(hexagon, hit.point);
                _hexagonGroup = _hexagonController.FindHexagonGrup(cornerName, hexagon);
                FrameImageDirection(cornerName);
                return (_hexagonGroup[0].MyPos + _hexagonGroup[1].MyPos + _hexagonGroup[2].MyPos) / 3;
            }
        }

        return Vector3.zero;
    }
    public Hexagon[] SelectGroup()
    {
        RaycastHit2D hit = RayToScreen(touchPos);
        Debug.DrawLine(Camera.main.ScreenToWorldPoint(touchPos), hit.point, Color.red, 10);

        if (hit.collider != null)
        {
            string[] splitRowAndColumn = hit.collider.name.Split('-');
            Hexagon hexagon = _hexagonController._hexagon[int.Parse(splitRowAndColumn[0]), int.Parse(splitRowAndColumn[1])];

            string cornerName = _hexagonController.FindNearestCorner(hexagon, hit.point);
            Hexagon[] _hexagonGroup = _hexagonController.FindHexagonGrup(cornerName, hexagon);

            return _hexagonGroup;
        }

        return null;
    }
    public void FrameImageDirection(string cornerName)
    {
        if (cornerName == "0" || cornerName == "2" || cornerName == "4")
        {
            _frameImageDirection = "L";
        }
        else
        {
            _frameImageDirection = "R";
        }
    }
    private string WhichDirectionRotate(Vector3 direction)
    {
        if (direction.x < 0 && direction.x < 0)
            return "right";
        if (direction.x > 0 && direction.x < 0)
            return "right";
        if (direction.x > 0 && direction.x > 0)
            return "left";
        if (direction.x < 0 && direction.x > 0)
            return "left";
        return string.Empty;
    }
    public void KillMethod()
    {
        LeanTouch.OnFingerUp -= OnFingerUp;
        LeanTouch.OnFingerDown -= OnFingerDown;
        LeanTouch.OnFingerSet -= OnFingerSet;
    }
}
